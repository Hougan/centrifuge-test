const { CentrifugeAPI } = require("./src/classes/CentrifugeAPI");
const { CentrifugeProxy } = require("./src/classes/CentrifugeProxy");

const API = new CentrifugeAPI('http://localhost:8000/api', '0c219a2e-378c-415a-bef8-0ea773a65f69');
const Proxy = new CentrifugeProxy(3000);

Proxy.host();

setInterval(() => {
  API.publish('presense:testInterval', {
    data: 'interval',
    success: true
  });
}, 1500);

Proxy.on('testRPC', (data, user) => {
  API.publish('public:testPublish', {
    data: '123',
    success: true
  });

  return {
    ok: 'ok? or ne ok?'
  }
})


Proxy.on('errorRPC', (data, user) => {
  
  return null
})

/**
 * Base API Response
 */
interface CentrifugoResponse {
  data: any | null;

  error: ErrorData | null;
}

interface CentrifugoEmptyResponse extends CentrifugoResponse {
  data: undefined;
}

interface CentrifugoPresenseResponse extends CentrifugoResponse {
  data: {
    presence: {
      [key: string]: {
        client: string,
        user: string
      }
    }
  }
}

interface CentrifugoPresenseStatsResponse extends CentrifugoResponse {
  data: {
    num_clients: number,
    num_users: number
  }
}

interface CentrifugoHistoryResponse extends CentrifugoResponse {
  data: {
    publications: {
      data: {
        text: string
      },
      uid: string
    }[]
  }
}

interface CentrifugoInfoResponse extends CentrifugoResponse {
  data: {
    nodes: {
      name: string,
      num_channels: number,
      num_clients: number,
      num_users: number,
      uid: string,
      uptime: number,
      version: string
    }[]
  };
}

interface CentrifugoChannelsResponse extends CentrifugoResponse {
  data: {
    channels: string[]
  }
}

/**
 * Base error data
 */
interface ErrorData {
  code: number;
  message: string;
}

type CentrifugoComposite = CentrifugoResponse | CentrifugoChannelsResponse | CentrifugoInfoResponse | CentrifugoHistoryResponse | CentrifugoPresenseStatsResponse | CentrifugoPresenseStatsResponse;
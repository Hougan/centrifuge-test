const { default: axios } = require("axios");

class CentrifugeAPI {
  /**
   * Class constructor
   * @param {string} host
   * @param {string} token 
   */
  constructor(host, token) {
    this.host = host;
    this.token = token;
  }

  /**
   * Publish message to single channel
   * @param {string} channel 
   * @param {any} obj 
   * @returns {Promise<CentrifugoEmptyResponse>}
   */
  async publish(channel, obj) {
    const result = await this.request('publish', {
      channel: channel,
      data: obj
    });

    return result;
  }

  /**
   * Publish message to multiply channe
   * @param {string[]} channels 
   * @param {any} obj 
   * @returns {Promise<CentrifugoEmptyResponse>}
   */
  async broadcast(channels, obj) {
    const result = await this.request('broadcast', {
      channels: channels,
      data: obj
    });

    return result;
  }

  /**
   * Disconnect user by his ID
   * @param {string} channel 
   * @param {string} userId 
   * @returns {Promise<CentrifugoEmptyResponse>}
   */
  async unsubscribe(channel, userId) {
    const result = await this.request('unsubscribe', {
      channel: channel,
      user: userId
    });

    return result;
  }

  /**
   * Disconnect user by his ID
   * @param {string} userId 
   * @returns {Promise<CentrifugoEmptyResponse>}
   */
  async disconnect(userId) {
    const result = await this.request('disconnect', {
      user: userId
    });

    return result;
  }

  /**
   * Get users of chanel
   * @param {string} channel 
   * @returns {Promise<CentrifugoPresenseResponse>}
   */
  async presense(channel) {
    const result = await this.request('presence', {
      channel: channel
    });

    return result;
  }

  /**
   * Get statistics of chanel
   * @param {string} channel 
   * @returns {Promise<CentrifugoPresenseStatsResponse>}
   */
  async presenseStats(channel) {
    const result = await this.request('presence_stats', {
      channel: channel
    });

    return result;
  }

  /**
   * Get histofy of channel publishes
   * @param {string} channel 
   * @returns {Promise<CentrifugoHistoryResponse>}
   */
  async history(channel) {
    const result = await this.request('history', {
      channel: channel
    });

    return result;
  }

  /**
   * Get info about created channels
   * @returns {Promise<CentrifugoChannelsResponse>}
   */
  async channels() {
    const result = await this.request('channels');

    return result;
  }

  /**
   * Get info about centrifugo instances
   * @returns {Promise<CentrifugoInfoResponse>}
   */
  async info() {
    const result = await this.request('info');

    return result;
  }

  /**
   * Base request to API
   * @param {string} method
   * @param {any} params
   * @returns {Promise<CentrifugoResponse|CentrifugoEmptyResponse|CentrifugoHistoryResponse|CentrifugoChannelsResponse|CentrifugoPresenseResponse|CentrifugoPresenseStatsRespons|CentrifugoInfoResponse>} 
   */
  async request(method, params = {}) {
    try {
      const response = await axios.post(this.host, {
        method: method,
        params: params
      }, {
        headers: {
          'Authorization': `apikey ${this.token}`
        }
      });
  
      const { result, error } = response.data;
      return {
        data: result,
        error: error
      };
    }
    catch (exc) {

    }
  }
}

module.exports = { CentrifugeAPI };
const Express = require('express');
const { Events } = require('@osmium/events');

class CentrifugeProxy extends Events {
  /**
   * Class constructor
   * @param {number} port 
   */
  constructor(port) {
    super();

    this.app = Express();

    this.port = port;
    this.useMiddleware();
  }

  useMiddleware() {
		this.use((name, ctx, ...args) => {
			if (ctx.isInject) return args;

			return {
        ret: false
      };
		});
  }

  /**
   * Host callback-proxy server
   */
  host() {
    this.app.use(Express.urlencoded({ extended: true }));
    this.app.use(Express.json());

    this.listenRoutes();

    this.app.listen(this.port);
  }

  listenRoutes() {
    this.app.post('/api/rpc', (...args) => this.onHttpRPC(...args));
  }

  /**
   * Fires when centrifuge ask for HTTP RPC response
   */
  async onHttpRPC(req, res) {
    const { method, user, data } = req.body;
    
    const resultData = await this.emitEx(method, true, {
      isInject: true
    }, {
      _userData: user,
      ...data
    });

    const result = resultData[Object.keys(resultData)[0]];
    console.log(result);
    if (!result) {
      throw new Error('fuck me');
    }

    res.json({
      result: {
        data: result
      } 
    })
  }
}

module.exports = { CentrifugeProxy };